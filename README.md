# Introduction to Ansible

This document provides an overview of Ansible, an open-source automation platform used for IT tasks such as configuration management, application deployment, intra-service orchestration, and provisioning.

## What is Ansible?

Ansible is a powerful automation tool that simplifies complex automation challenges. It is designed to be minimal in nature, consistent, secure, and highly reliable, with an extremely low learning curve for administrators, developers, and IT managers.

### Key Features of Ansible

- **Agentless Architecture**: Unlike other management tools, Ansible does not require any agent software to be installed on nodes it manages. It leverages SSH or PowerShell to communicate with the remote hosts.

- **Idempotency**: The ability of Ansible to ensure that an operation will always produce the same result, regardless of the system's state or the number of times the operation is applied.

- **Declarative Language**: Ansible uses YAML for its playbook language, which is easy to read, write, and share.

- **Modules**: Ansible works by connecting to your nodes and pushing out small programs called "modules" to them. These modules are executed remotely and then removed when finished.

### Use Cases for Ansible

- **Configuration Management**: Automate the configuration of systems and services in your infrastructure.

- **Application Deployment**: Seamlessly deploy applications across various environments, from development to production.

- **Continuous Delivery**: Create a pipeline that enforces your desired state of deployments.

- **Provisioning**: Spin up environments quickly and consistently, from physical servers to cloud or virtualized environments.

- **Security Automation**: Apply security policies and compliance checks across your infrastructure with ease.

- **Orchestration**: Coordinate complex multi-tier deployments across different environments.

## Getting Started with Ansible

To start using Ansible, you need to install it on a control machine (which orchestrates the automation), and then it can manage an entire fleet of remote machines from that central point.

### Python Requirement

Most Ansible modules require Python on the managed nodes. For Linux-based systems, Python 2 (version 2.7) or Python 3 (version 3.5 or higher) is required. For Windows nodes, PowerShell 3.0 or higher is needed.

- Ensure Python is installed on your Linux-based remote hosts for Ansible to function correctly.
- For managing Windows nodes, set up PowerShell remoting and ensure appropriate WinRM configurations.

### User Permissions

- The control machine needs SSH access to the remote hosts. For Linux hosts, Ansible typically connects as the `root` user or another user with sudo privileges.
- Ensure the user Ansible connects with has sufficient permissions to execute the required tasks. You may use key-based SSH authentication and configure `sudo` for passwordless escalation if necessary.
- For Windows nodes, administrative privileges are typically required.

Ansible's simplicity, powerful features, and scalability make it a popular choice for automating and managing IT infrastructure.

---

For more information on Ansible, visit the [official Ansible documentation](https://docs.ansible.com/).

# Ansible modules and Ansible ad hoc commands

Ansible modules are scripts to do a specific task at managed nodes. For example, there are modules to check availability, copy files, install applications, and lots more. To get the full list of modules, you can check the [official Ansible modules page](https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html).

A quick way to use Ansible modules is with an ad hoc command. Ad hoc commands use the ansible command-line interface to execute modules at the managed nodes. The usage is as follows:

```bash
ansible <pattern> -m <module> -a "<module options>" -i <inventory> -u <user> --private-key <private_key>
```

Example on local:
```bash
ansible localhost -m ping -i "localhost," -c local
```

Where:

- `<pattern>` is the IP address, hostname, alias or group name,
- `-m <module>` is name of the module to be used,
- `-a "<module options>"` sets options for the module, and
- `-i <inventory>` is the inventory of the managed nodes.
- `-u <user>` is the user that access to remote host(s)
- `--private-key <private_key>` is the private key to connect to remote host(s)

## Ansible Command Examples

- **Checking Machine Status**:
  - Command: `ansible all -m ping -i inventory.ini`
  - Purpose: Verify connectivity with all managed machines.

- **Executing Remote Commands**:
  - Command: `ansible mynginx -m shell -a 'uptime' -i myinventory.ini -u ec2-user --private-key /path/to/private_key.pem`
  - Purpose: Execute the `uptime` command on servers in the `mynginx` group.

- **Managing Users**:
  - Command: `ansible all -m user -a 'name=example_user state=present' --become`
  - Purpose: Create a user on all machines.
  - Note: `--become` flag in Ansible is used to enable privilege escalation. It tells Ansible to run the specified task with elevated privileges, similar to sudo in Unix/Linux systems. `state=present` indicates that the user named example_user should exist on the target systems. If this user does not exist, Ansible will create it.

- **Updating Packages**:
  - Command: `ansible all -m apt -a 'update_cache=yes upgrade=safe' --become`
  - Example on local : `ansible localhost -m apt -a 'update_cache=yes upgrade=safe' -i "localhost," -c local --become --ask-become-pass`
  - Purpose: Update packages on Debian/Ubuntu systems.

# Ansible Playbooks

Ansible Playbooks are YAML files for orchestrating multi-tier IT environments. They define the desired state of systems, services, and applications.

## Key Components of a Playbook

- **Hosts**: Target machines.
- **Tasks**: Actions using Ansible modules.
- **Variables**: Manage system differences.
- **Handlers**: Special tasks, often for service restarts.
- **Roles**: Reusable task sets.

## Playbook Examples

In order to run ansible playbook on local, you can use this command

```bash
ansible-playbook playbook.yml -i "localhost," -c local 
```

### Variable Storing and Reusing in Ansible

This playbook demonstrates how to store and reuse variables:

```yaml
---
- name: Variable Storing and Reusing Example
  hosts: all
  tasks:
    - name: Set a variable using set_fact
      set_fact:
        new_variable: "Example Value"

    - name: Use the variable in another task
      debug:
        msg: "The value of new_variable is {{ new_variable }}"

    - name: Get current date and store it
      command: date +%Y%m%d
      register: current_date

    - name: Display the date in another task
      debug:
        msg: "The date is {{ current_date.stdout }}"
```

- `set_fact`: This module is used to define new variables. In the example, new_variable is set to "Example Value".
- `register`: Used to capture the output of a command. Here, current_date captures the output of the date command, which can then be used in subsequent tasks.

### Installing and Managing Apache

- Sets up Apache on web servers.
- Updates packages, installs Apache, ensures it's running.

```yaml
---
- name: Verify apache installation
  hosts: webservers
  vars:
    http_port: 80
    max_clients: 200
  remote_user: root
  tasks:
    - name: Ensure apache is at the latest version
      ansible.builtin.yum:
        name: httpd
        state: latest

    - name: Write the apache config file
      ansible.builtin.template:
        src: /srv/httpd.j2
        dest: /etc/httpd.conf
      notify:
      - Restart apache

    - name: Ensure apache is running
      ansible.builtin.service:
        name: httpd
        state: started

  handlers:
    - name: Restart apache
      ansible.builtin.service:
        name: httpd
        state: restarted
```

### User Management Playbook

- Manages user accounts on target machines.
- Can create or remove users and manage their attributes.

```yaml
---
- name: Manage User Accounts
  hosts: all
  tasks:
    - name: Ensure user is present
      ansible.builtin.user:
        name: example_user
        state: present
        password: <encrypted_password>
        groups: "sudo,users"

    - name: Remove user
      ansible.builtin.user:
        name: obsolete_user
        state: absent
```

### Database Setup

- Installs and configures databases like MySQL.
- Manages database users and permissions.

Playbooks automate complex workflows, ensuring consistent configurations across environments.

```yaml
---
- name: MySQL Database Setup
  hosts: db_servers
  tasks:
    - name: Install MySQL
      ansible.builtin.yum:
        name: mysql-server
        state: present

    - name: Start MySQL Service
      ansible.builtin.service:
        name: mysqld
        state: started

    - name: Create database user
      mysql_user:
        name: db_user
        password: <user_password>
        priv: '*.*:ALL'
        state: present
```

# Hands On: Deploying Apache Server on AWS EC2 using Terraform and Ansible through Ansible Playbook

This section guides you through the steps to deploy an Apache server on an AWS EC2 instance using Terraform for infrastructure provisioning and Ansible for configuration management.

## Terraform Setup

The Terraform configuration consists of several files in the `1-Basic-Playbook` directory:

- `00-providers.tf`: Defines the required providers (AWS, TLS, and HTTP).
- `01-key.tf`: Generates an RSA private key and creates an AWS key pair for SSH access.
- `02-ec2.tf`: Provisions an EC2 instance and a security group with specific rules for HTTP, HTTPS, and SSH access.
- `98-datas.tf`: Retrieves the latest Amazon Linux AMI for 2023 and your current public IP address.
- `99-outputs.tf`: Outputs necessary details such as the public IP of the EC2 instance.

### Steps to Provision Infrastructure

1. **Initialize Terraform**:
   Navigate to the `1-Basic-Playbook` directory and run `terraform init` to initialize the working directory.

2. **Apply Terraform Configuration**:
   Run `terraform apply` to create the AWS infrastructure. Confirm the action by typing `yes` when prompted.

3. **Retrieve Outputs**:
   After successful application, Terraform will output the public IP of the EC2 instance.

## Ansible Configuration

In this project, Ansible is used to configure and manage the AWS EC2 instance. The setup includes a script to generate an Ansible inventory file and a playbook to install and start the Apache server.

### Generating the Inventory File

The `setting_host.sh` script automates the creation of the Ansible inventory file by extracting the public IP address of the EC2 instance from Terraform's outputs.

- **Script**: `setting_host.sh` reads the EC2 instance's public IP address from the Terraform outputs and generates an `ansible_hosts` file with this IP under the `[ec2_instance]` group.

### Ansible Playbook

The `playbook.yml` defines the tasks to be executed on the EC2 instance. It includes updating packages, installing the Apache server, and ensuring that the Apache service is running.

- **Playbook Tasks**:
  - **Update Package**: Updates all packages on the instance using the YUM package manager.
  - **Install Apache (httpd)**: Installs the Apache server.
  - **Start Apache Service**: Ensures the Apache service is started and set to run at boot.

- **Variables**:
  - `ansible_ssh_private_key_file`: Path to the SSH private key generated by Terraform.
  - `ansible_user`: The default user for the Amazon Linux 2 AMI.

### Running the Playbook

1. **Execute the Script**: Run `./setting_host.sh` to generate the inventory file.

2. **Run the Playbook**: Use the command `ansible-playbook -i ansible_hosts playbook.yml` to apply the configuration to the EC2 instance.

By following these steps, the EC2 instance will be configured with the latest updates and have the Apache server installed and running.

Note: Ensure that the control machine has Ansible installed and is configured to use the SSH private key for connections.

---

### Exercise: Create an `index.html` File using an Ansible Template

## Objective

Modify the existing `playbook.yml` to add a task that creates an `index.html` file in the `/var/www/html` directory on the EC2 instance using an Ansible template.

## Tasks

### 1. Create a Template File

- In your Ansible project directory, create a new HTML template file named `index.html.j2`. This Jinja2 template file will be used to generate the `index.html`.
- Design a simple HTML page layout in the `index.html.j2` template. You can include elements like a heading, a paragraph, and placeholders for dynamic content.

### 2. Add a New Task in the Playbook

- Modify `playbook.yml` to add a new task that uses the Ansible `template` module to deploy the `index.html.j2` file to the `/var/www/html` directory on the EC2 instance.
- Ensure the task sets appropriate file permissions for the `index.html`.

### 3. Include Variables (Optional)

- For added complexity, define variables in the playbook or in a separate variables file (`vars/main.yml`), and use them in the `index.html.j2` template. Variables could include items like a title, a welcome message, or any other dynamic content.

### 4. Test Your Configuration

- Run the modified playbook to apply the changes.
- Verify that the `index.html` file is correctly created on the EC2 instance and contains the content from your template.
