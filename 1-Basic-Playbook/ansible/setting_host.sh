#!/bin/bash

# Read IP address from Terraform outputs
cd ../terraform
EC2_PUBLIC_IP=$(terraform output -raw ec2_public_ip)
cd -

# Generate Ansible inventory file
echo "[ec2_instance]" > ansible_hosts
echo $EC2_PUBLIC_IP >> ansible_hosts