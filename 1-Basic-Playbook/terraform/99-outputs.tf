output "ec2_public_ip" {
  description = "L'adresse IP publique de l'instance EC2"
  value       = aws_instance.example.public_ip
}