resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_key_pair" "deployer_key" {
  key_name   = "test_key"
  public_key = tls_private_key.key.public_key_openssh
}

resource "local_sensitive_file" "private_key" {
  content  = tls_private_key.key.private_key_pem
  filename = "${path.module}/key.pem"

  provisioner "local-exec" {
    command = "chmod 600 ${self.filename}"
  }
}