resource "aws_instance" "example" {
  ami             = data.aws_ami.amazon-2023.id
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.deployer_key.key_name
  security_groups = [aws_security_group.example_sg.name]

  tags = {
    Name = "Ansible"
  }
}

resource "aws_security_group" "example_sg" {
  name        = "example_security_group"
  description = "Security group for EC2 instance with HTTP, HTTPS, and SSH access"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP flow rule
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS flow rule
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # SSH flow rule
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.my_ip.body)}/32"] # Chomp remove end character
  }
}