data "aws_ami" "amazon-2023" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023-ami-*-x86_64"]
  }
  owners = ["amazon"]
}

data "http" "my_ip" {
  url = "http://ipv4.icanhazip.com"
}